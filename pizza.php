<?php

include_once 'function/funciones.php';
include 'head.php';

// to prevent undefined index notice
$action = isset($_GET['action']) ? $_GET['action'] : "";
$product_id = isset($_GET['product_id']) ? $_GET['product_id'] : "1";
$name = isset($_GET['name']) ? $_GET['name'] : "";
$cantidad = isset($_GET['cantidad']) ? $_GET['cantidad'] : "0";

if ($action == 'limpiarPedido') {
    $_SESSION['id_pedido'] = "";
}

//echo 'id sesion' . $_SESSION['id_pedido'];

$funciones = new funciones();
$listaProductos = $funciones->getIngredientes();
$listaPizzas = $funciones->getPizzas();


$product_order = $funciones->getIngList();
$total = $funciones->getTotal();
$totalPizza = $total + ($total / 2);
$totalPizzaBase = ($totalPizza - $total);

echo "<img src='http://www.noticias-frescas.com/thumb/5/242946.jpg' alt'' title'' />";
		
echo '<br><br>';
//PIZZAS
if (count($listaPizzas) > 0) {
//start table
    echo "<table class='table table-hover table-responsive table-bordered' name=productosGrid>";
    echo "<tr><td style='max-width: 150px';>";
    echo "<div class = 'bottommargin-sm'>";
    echo "Pizzas: <select name='pizza' id='pizza' class = 'select-1 form-control select2-hidden-accessible' style = 'width:100%;' tabindex = '-1' aria-hidden = 'true'>";
    foreach ($listaPizzas as $pizza) {
        echo "<option value=" . $pizza['cod'] . ">" . $pizza['nombre'] . "</option>";
    }
    echo "</select>";
    echo "</div>";
    echo "</td>";
    echo "<td >";
    echo "<button class='btn btn-primary agregarPizza' style='margin-top:20px'>";
    echo "<span class='glyphicon glyphicon-shopping-cart'></span> Agregar Pizza";
    echo "</button>";
    echo "</td>";
    echo "</tr>";

    
} else {
    echo "No hay Pizzas.";
}

//LISTA DE INGREDIENTES
if (count($listaProductos) > 0) {
//start table
    echo "<table class='table table-hover table-responsive table-bordered' name=productosGrid>";
    echo "<tr><td style='max-width: 150px';>";
    echo "<div class = 'bottommargin-sm'>";
    echo "Adicionales: <select name='productos' id='productos' class = 'select-1 form-control select2-hidden-accessible' style = 'width:100%;' tabindex = '-1' aria-hidden = 'true'>";
    foreach ($listaProductos as $producto) {
        echo "<option value=" . $producto['cod'] . ">" . $producto['nombre'] . ' $ ' . $producto['precio'] . "</option>";
    }
    echo "</select>";
    echo "</div>";
    echo "</td>";

    echo "<td style='width: 150px;'>";
    echo "Cantidad: <input type='number' id='cant' name='quantity' value='1' class='form-control' />";
    echo "</td>";
    echo "<td >";
    echo "<button class='btn btn-primary agregarIngrediente' style='margin-top:20px'>";
    echo "<span class='glyphicon glyphicon-shopping-cart'></span> Agregar";
    echo "</button>";
    echo "</td>";
    echo "</tr>";

    if (count($product_order) > 0) {
        echo "<table class='table table-hover table-responsive table-bordered'>";
        echo "<tr>";
        echo "<th class='textAlignLeft'>Adicionales</th>";
        echo "<th style='width:5em;'>Cantidad</th>";
        echo "<th style='width:5em;'>Precio</th>";
        echo "</tr>";
        foreach ($product_order as $prodOrder) {
            echo "<tr>";
            echo "<td style='border-top: 1px solid #dddddd'>";
            echo "<div class='product-id' style='display:none;'>{$prodOrder['id']}</div>";
            echo "<div class='product-name'>" . $prodOrder['nombre'] . "</div>";
            echo "</td>";

            echo "<td style='border-top: 1px solid #dddddd'>";
            echo "<div >";
            echo "<div class='product-name'>" . $prodOrder['cantidad'] . "</div>";
            echo "</div>";
            echo "</td>";
            echo "<td style='border-top: 1px solid #dddddd'>";
            echo "<div class='product-precio'>" . $prodOrder['precio'] . "</div>";
            echo "</td>";
            echo "<td style='border-top: 1px solid #dddddd'>";
            echo "<span class='input-group-btn'>";

            echo "<a>";
            echo "<button class='btn btn-danger cambiarEstado' type='button'>Quitar</button></a>";
            echo "</span>";
            echo "</td>";
            echo "</tr>";
        }
        echo "<tr>";
        echo "<th class='' coldspan='3'>Precio Pizza Base: {$totalPizzaBase}</th>";
        echo "</tr>";
        echo "<tr>";
        echo "<th class='textAlignLeft'>Precio Total: {$totalPizza}</th>";
        echo "</tr>";
        echo "</table>";
    }
    echo "</table>";
} else {
    echo "No hay pizzas encontradas.";
}
include 'footer.php';
