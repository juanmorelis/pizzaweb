<?php

session_start();

include_once 'function/funciones.php';
$funciones = new funciones();

//Obtengo el ultimo id de la tabla pedidos +1
//Voy creando pre pedidos sin confirmar para luego cambiar el estado a confirmado
$maxIdPedido = $funciones->getMaxIdPedido();

//Capturo el ID de pizza seleccionado
$idPizza = isset($_GET['id']) ? $_GET['id'] : die;
//Obtengo los productos de una pizza por default, y genero el pre pedido
$ingPizzaBase = $funciones->getIngPizzaXID($idPizza);

foreach ($ingPizzaBase as $in) {
    $funciones->insertarPrePizza($in['id'], $maxIdPedido);
}
header('Location: pizza.php');
