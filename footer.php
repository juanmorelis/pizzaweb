</div>

<!-- Footer
        ============================================= -->
<footer id="footer" class="dark">
    <div class="container">
        <i class="icon-envelope2"></i> pizzaApp@pizzaweb.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 1111-1111 <span class="middot">&middot;</span> <i class="icon-wei-plain icon-globe"></i><a href="http://www.pizzaweb.com"> www.pizzaweb.com </a> </div><!-- #copyrights end -->
</footer><!-- #footer end -->

<!-- /container -->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- bootstrap JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.js"></script>
<script src="js/components/select-boxes.js"></script>
<script src="js/components/selectsplitter.js"></script>

<script>
    $(document).ready(function () {
        $('.agregarIngrediente').click(function () {
            var id = document.getElementById("productos").value;
            
            var quantity = document.getElementById("cant").value;
            window.location.href = "agregar.php?id=" + id + "&cantidad=" + quantity;
        });
       
        $('.agregarPizza').click(function () {
            var id = document.getElementById("pizza").value;
            window.location.href = "agregarPizza.php?id=" + id;
        });
        $('.limpiarPedido').click(function () {
            window.location.href = "pizza.php?action=limpiarPedido";
        });       
        $('.confirmarPedido').click(function () {
            var comentario = document.getElementById("obs").value;
            window.location.href = "confirmarPedido.php?obs=" + comentario;
        });
        
        $('.cambiarEstado').click(function () {
            var id = $(this).closest('tr').find('.product-id').text();
            
            var parametros = {
                "id": id
            };
            console.log(parametros['id']);
            $.ajax({
                data: parametros,
                url: "eliminarIng.php",
                type: "post"

            }).done(function (respuesta) {
                console.log(respuesta);
                window.location.replace("http://45.55.92.82/pizzaweb/pizza.php");
            });
        });
        
        // Multiple Select
        $(".select-1").select2({
            placeholder: "Select Multiple Values"
        });
        // Loading array data
        var data = [{id: 0, text: 'enhancement'}, {id: 1, text: 'bug'}, {id: 2, text: 'duplicate'}, {id: 3, text: 'invalid'}, {id: 4, text: 'wontfix'}];
        $(".select-data-array").select2({
            data: data
        })
        $(".select-data-array-selected").select2({
            data: data
        });
        // Enabled/Disabled
        $(".select-disabled").select2();
        $(".select-enable").on("click", function () {
            $(".select-disabled").prop("disabled", false);
            $(".select-disabled-multi").prop("disabled", false);
        });
        $(".select-disable").on("click", function () {
            $(".select-disabled").prop("disabled", true);
            $(".select-disabled-multi").prop("disabled", true);
        });
        // Without Search
        $(".select-hide").select2({
            minimumResultsForSearch: Infinity
        });
        // select Tags
        $(".select-tags").select2({
            tags: true
        });
        
        
       
        
        
        

    });
</script>

</body>
</html>