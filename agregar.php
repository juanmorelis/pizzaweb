<?php

session_start();
include_once 'function/funciones.php';
$funciones = new funciones();

//Obtengo el ultimo id de la tabla pedidos +1
//Voy creando pre pedidos sin confirmar para luego cambiar el estado a confirmado
if (isset($_SESSION['id_pedido'])){
    $maxIdPedido = $_SESSION['id_pedido'];
}else{
    $maxIdPedido = $funciones->getMaxIdPedido();
}


$id_ingrediente = isset($_GET['id']) ? $_GET['id'] : die;
$cantidad = isset($_GET['cantidad']) ? $_GET['cantidad'] : die;


$funciones->insertarPrePizzaIng($id_ingrediente, $maxIdPedido,$cantidad);

header('Location: pizza.php');