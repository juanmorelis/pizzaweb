<?php

session_start();
include_once 'includes/conect.php';

class funciones {

    private $db;
    private $mysqli;
    private $datosSesion;
    private $lista_Ingredientes;
    private $ingList;
    private $total = 0;
    private $lista_pizzas;

    public function __construct() {
        $this->db = Database::getInstance();
        $this->mysqli = $this->db->getConnection();

//        $this->datosSesion = $_SESSION;
    }

    public function getIngredientes() {
        $this->lista_Ingredientes = $this->getIng();
        return $this->lista_Ingredientes;
    }

    public function getPizzas() {
        $this->lista_pizzas = $this->getPizzasListas();
        return $this->lista_pizzas;
    }

    private function getPizzasListas() {
        $query = "SELECT * from pizzas";
        if ($stmt = $this->mysqli->prepare($query)) {
//            $stmt->bind_param("ii", $_SESSION['id_usuario'], $_SESSION['id_cliente']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $status);

            while ($stmt->fetch()) {
                $listPizzas[] = array("cod" => $id, "nombre" => $nombre, "status" => $status);
            }
        }
        return $listPizzas;
    }

    public function getTotal() {

        $this->total = $this->getTotalPre();
        return $this->total;
    }

    private function getIng() {
        $query = "SELECT * from Ingredientes";
        if ($stmt = $this->mysqli->prepare($query)) {
//            $stmt->bind_param("ii", $_SESSION['id_usuario'], $_SESSION['id_cliente']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($Id, $nombre, $precio);

            while ($stmt->fetch()) {
                $listProd[] = array("cod" => $Id, "nombre" => $nombre, "precio" => $precio);
            }
        }
        return $listProd;
    }

    private function getIngOrder() {


        $query = "Select p.id_pedido id, i.nombre nombre,p.cantidad cantidad,i.precio precio
        from pedidos p
        Inner join Ingredientes i ON i.id = p.id_ingrediente
        where p.id_pedido = ?
        ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $_SESSION['id_pedido']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $cantidad, $precio);

            while ($stmt->fetch()) {
                $prodIngList[] = array("id" => $id, "nombre" => $nombre, "cantidad" => $cantidad, "precio" => $precio);
            }
        }

        return $prodIngList;
    }

    private function getTotalPre() {

        $query = "SELECT SUM(p.cantidad*i.precio) as total
		from pedidos p 
		Inner join Ingredientes i ON i.Id = p.id_ingrediente
		where p.id_pedido = ?
        ";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $_SESSION['id_pedido']);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($total);
            $stmt->fetch();
        }

        return $total;
    }

    public function getIngList() {
        $this->ingList = $this->getIngOrder();
        return $this->ingList;
    }

    public function guardarPedidoS($ingrediente) {
        $this->guardoPedido($ingrediente);
    }

    public function getMaxIdPedido() {
        return $this->getMaxIDPedidos();
    }

    private function getMaxIDPedidos() {
        $queryP = "SELECT  MAX(id_pedido) id_pedidoMax 
                    FROM pedidos";

        $stmt = $this->mysqli->prepare($queryP);

        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($id_pedidoMax);

        $stmt->fetch();
        if ($id_pedidoMax > '0') {
            $id_pedidoMax = $id_pedidoMax + 1;
            $id_pedido = $id_pedidoMax;
            $_SESSION['id_pedido'] = $id_pedidoMax;
        } else {
            $id_pedido = "1";
            $_SESSION['id_pedido'] = $id_pedido;
            //        die("NO ENCONTRO PEDIDO");
        }
        return $id_pedido;
    }

    public function getIngPizzaXID($idPizza) {
        return $this->getIngPizzaId($idPizza);
    }

    private function getIngPizzaId($idPizza) {


        $query = "SELECT i.id id, i.nombre nombre,i.precio precio 
                FROM pizzasListas p
                INNER JOIN Ingredientes i ON i.Id = p.id_ingrediente
                WHERE p.id_pizza = ?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("i", $idPizza);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($id, $nombre, $precio);

            while ($stmt->fetch()) {
                $ingPizzaID[] = array("id" => $id, "nombre" => $nombre, "cantidad" => $cantidad, "precio" => $precio);
            }
        }

        return $ingPizzaID;
    }

    public function insertarPrePizza($idIngrediente, $idPedido) {
        return $this->insertarPreOrder($idIngrediente, $idPedido);
    }

    private function insertarPreOrder($idIngrediente, $idPedido) {

        $query = "INSERT INTO pedidos SET id_pedido=?, id_ingrediente=?,cantidad=1";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ii", $idPedido, $idIngrediente);
            $stmt->execute();
            $stmt->store_result();
            
        }
    }
    public function insertarPrePizzaIng($idIngrediente, $idPedido,$cantidad) {
        return $this->insertarPreOrderIng($idIngrediente, $idPedido,$cantidad);
    }

    private function insertarPreOrderIng($idIngrediente, $idPedido,$cantidad) {

        $query = "INSERT INTO pedidos SET id_pedido=?, id_ingrediente=?,cantidad=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("iii", $idPedido, $idIngrediente,$cantidad);
            $stmt->execute();
            $stmt->store_result();
            
        }
    }
    public function deleteIng($idIngrediente, $idPedido) {
        return $this->deletePreOrderIng($idIngrediente, $idPedido);
    }

    private function deletePreOrderIng($idIngrediente, $idPedido) {

        $query = "DELETE FROM pedidos WHERE id_pedido=? AND id_ingrediente=?";

        if ($stmt = $this->mysqli->prepare($query)) {
            $stmt->bind_param("ii", $idPedido, $idIngrediente);
            $stmt->execute();
            $stmt->store_result();
            
        }
    }

}
